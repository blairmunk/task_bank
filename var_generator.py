import random
import math

def read_task_groups(file):
    """Split a file to groups with tasks. Add a Return a list of groups with list of tasks"""
    with open(file, 'r', encoding='utf-8') as f:
        lines = f.readlines()
        
    task_groups = []
    current_group = []
    for line in lines:
        if line.startswith('==='): # Go to the next group of tasks
            if current_group:
                task_groups.append(current_group)
                current_group = []
        else:
            current_group.append(line.strip())
    
    if current_group:
        task_groups.append(current_group)
    
    return task_groups

def select_tasks_from_groups(task_groups, factor):
    selected_tasks = []
    for group in task_groups:
        num_tasks_in_group = len(group)
        num_tasks_to_select = math.ceil(num_tasks_in_group / factor)
        selected = random.sample(group, num_tasks_to_select)
        selected_tasks.extend(selected)
    return selected_tasks

def split_tasks(tasks_list):
    """Split a task to problems and answers. Return list of lists(problem, answer)"""
    splitted_tasks = []
    for task in tasks_list:
        task = task.strip().split('||')
        splitted_tasks.append(task)
    return splitted_tasks

def generate_latex_vars(variant_list):
    template = r'''\input{preamble_vars}
\begin{document}
'''
    for i, variant in enumerate(variant_list):
        template += r"\begin{minipage}{\linewidth}" + '\n'
        template += fr"\begin{{multicols}}{{{num_of_cols_vars}}}"
        template += fr'''[
            \subsection*{{Тема: {subject}}}
            {description}
            \begin{{textblock}}{{1}}(10.9,-1.2)
            \framebox{{Вариант {i+1}}}
            \end{{textblock}}
            ]
            '''

        template += r"\begin{enumerate}[noitemsep,topsep=0pt,parsep=5pt,partopsep=0pt]" + '\n'
        for j, task in enumerate(variant):
            problem = task[0].strip()
            template += r"\item" + f" {problem.strip()}" + '\n'
            #template += f"{j+1}. {task.strip()}\\\\ \n"
        template += r"\end{enumerate}" + '\n'
        template += r"\end{multicols}" + '\n'
        template += r'''
            \begin{textblock}{8}(6,0)
	        {\tiny Отметки учителя о выполнении: } \framebox{\phantom{!!!} } \ \framebox{\phantom{!!!} } \ \framebox{\phantom{!!!} }
            \end{textblock}
            '''
        template += r"\end{minipage}" + '\n\n'
        template += separators[sep_ind] + '\n'
    template += r'''\end{document}'''
    return template

def generate_latex_answers(variant_list):
    template = r'''\input{preamble_vars}
\begin{document}
'''
    template += fr'''\subsection*{{Ответы: {subject}}}'''
    for i, variant in enumerate(variant_list):
        template += r"\begin{minipage}{\linewidth}" + '\n'
        template += fr"\begin{{multicols}}{{{num_of_cols_answers}}}"
        template += fr'''[
            \subsubsection*{{Вариант {i+1}}}
            ]
            '''

        template += r"\begin{enumerate}[noitemsep,topsep=0pt,parsep=5pt,partopsep=0pt]" + '\n'
        for j, task in enumerate(variant):
            answer = task[1].strip()
            template += r"\item" + f" {answer.strip()}" + '\n'
            #template += f"{j+1}. {task.strip()}\\\\ \n"
        template += r"\end{enumerate}" + '\n'
        template += r"\end{multicols}" + '\n'
        template += r"\end{minipage}" + '\n\n'
        template += r'\vspace{2em}' + '\n'
    template += r'''\end{document}'''
    return template

tasks_file = 'tasks2.txt'  # Путь к файлу с заданиями
num_variants_to_generate = int(input('Сколько вариантов сгенерировать?\n'))
subject = input('Тема в заголовке к заданиям: ')
description = input('Описание к заданиям: ')
num_of_cols_vars = int(input('Разбивка на колонки. Сколько колонок у заданий: '))
num_of_cols_answers = int(input('Сколько колонок в ответах: '))
separators = [r'\newpage', r'\vspace{2em}\hrulefill\vspace{2em}']
sep_ind = int(input('Что использовать в качестве разделителя вариантов? \n 1. Новая страница \n 2. Линия-разделитель \n Введите 1 или 2: ')) - 1
factor = 3  # Фактор уменьшения количества заданий для варианта

task_groups = read_task_groups(tasks_file)
generated_variants = []
for _ in range(num_variants_to_generate):
    selected_tasks = select_tasks_from_groups(task_groups, factor)
    generated_variants.append(split_tasks(selected_tasks))

latex_vars = generate_latex_vars(generated_variants)

latex_answers = generate_latex_answers(generated_variants)

with open('tex/generated_variants.tex', 'w', encoding='utf-8') as f:
    f.write(latex_vars)

with open('tex/generated_answers.tex', 'w', encoding='utf-8') as f:
    f.write(latex_answers)